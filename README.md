# Search

Search provider. Currently an ElasticSearch Instance for fulltext queries

IMPORTANT: ElasticSearch requires this to be executed before starting the docker container:  `sysctl -w vm.max_map_count=262144`

## Environment
ElasticSearch SingleNode instance accessable via ElasticSearch [REST API](https://www.elastic.co/guide/en/elasticsearch/reference/current/rest-apis.html)

Needs a Query inside the query.rq file or .(nd)JSON formatted data.js file as datasource containing SparQL entities. Example Datasource: [Wikidata Dumps](https://dumps.wikimedia.org/wikidatawiki/) (Requiring large Diskspace)

Dumps can be shrunk using [wikibase-dump-filter](https://github.com/maxlath/wikibase-dump-filter/) (Requiring NodeJS and computation power)

Filled using [elastic-wikidata](https://github.com/TheScienceMuseum/elastic-wikidata) which is run in a Docker environment. (Requiring Python, a running ElasticSearch with Credentials & some bugfixes)

## Tools
ElasticSearch database visualisation via Elasticvue Browser addon: https://elasticvue.com/

## Setup 
- 1. Configurate connection using config.ini
- 2. Set your desired datasource in script.sh 
  - 1. Fulldump 
  - 2. Custom Data [data.json]
  - 3. Subdump 
  - 4. SparQL Query-Response [query.rq]
- 3. Build & Run Dockerfile
- 4. [Optional] Check your elastic using https://elasticvue.com/
