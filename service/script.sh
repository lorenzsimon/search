###########################################################
### This script is run inside the Docker environment    ###
### Use as entrypoint for seeding                       ###
### Environment includes Python and NodeJS              ###
###########################################################



### 1. GET FRESH FULL-DUMP FROM wikidata  
### Requires a temporary disc capacity of ~1.5TB 
# wget -C https://dumps.wikimedia.org/wikidatawiki/entities/latest-all.json.bz2
# bzip2 -d latest-all.json.bz2 > ./data.json
# ew dump -p ./data.json -c ./config.ini
# rm ./data.json
# rm ./latest-all.json.bz2


### 2. Insert custom data from data.json
# ew dump -p ./data.json -c ./config.ini --index entities


### 3. GET FRESH FULL-DUMP FROM wikidata AND USE ONLY subdump of it
### Requires a temporary disc capacity of ~1.5TB 
# wget -C https://dumps.wikimedia.org/wikidatawiki/entities/latest-all.json.bz2
# cat latest-all.json.bz2 | bzcat | wikibase-dump-filter --claim P31:Q5 > data.ndjson
# ew dump -p ./data.json -c ./config.ini
# rm ./data.ndjson
# rm ./latest-all.json.bz2


### 4. Insert data from a wikidata request defined in query.rq
ew query -p ./query.rq -c ./config.ini --index entities