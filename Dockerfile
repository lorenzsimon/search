FROM python:3
RUN apt update
RUN apt-get -y install nodejs
RUN apt-get -y install npm
RUN apt-get -y install bzip2
RUN apt-get -y install tree
RUN pip3 install elastic_wikidata
RUN npm install wikibase-dump-filter
COPY ./service .

# Copying fix for elastic wikidata
COPY ./fix ./usr/local/lib/python3.9/site-packages
RUN chmod +x .
CMD ew query -p ./query.rq -c ./config.ini --index test
